// stdlib functionality     
#include <iostream>
#include <fstream>

// ROOT functionality
#include <TFile.h>
#include <TH1D.h>

int main(int argc, char **argv) {

  // Open the input file
  TFile *f_in = new TFile(argv[1]);

  // Collect the histogram from the file                                                                                                                             
  TH1D * hist = (TH1D*)f_in->Get("h_mjj_kin_cal");

  // Write the bin edges and contents to the output file
  std::ofstream f_out(argv[2]);

  // First write the bin edges
  double bin_width = hist->GetBinWidth(1);   // Needed for computing last bin edge
  int n_bins = hist->GetNbinsX();

  for(int iBin=1; iBin < n_bins+1; iBin++)
  {
    f_out << hist->GetBinLowEdge(iBin) << " ";
  }

  f_out << hist->GetBinLowEdge(n_bins) + bin_width << std::endl;

  // Now write the bin contents
  for(int iBin=1; iBin < n_bins+1; iBin++)
  {
    f_out << hist->GetBinContent(iBin) << " ";
  }

  f_out.close();
}
